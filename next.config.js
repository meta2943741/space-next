/** @type {import('next').NextConfig} */
const nextConfig = {
  webpack(config, options) {
    config.module.rules.push({
      test: /\.(woff|woff2|eot|ttf|otf)$/,
      use: {
        loader: 'file-loader',
        options: {
          publicPath: '../fonts',
          name: '[name].[ext]',
        },
      },
    });

    return config;
  },
}

module.exports = nextConfig
