import React from 'react'

type Props = {
  title: string;
  children: React.ReactNode | null;
  url: string;
}

const LinkButton = ({
  title,
  children,
  url
}: Props) => {
  return (
    <a href={`/${url}`} className="inline-flex items-center justify-start cursor-pointer">
      {children}
      {title}
    </a>
  )
}
export default LinkButton