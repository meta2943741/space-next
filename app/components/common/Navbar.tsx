'use client'

import Link from 'next/link'
import { useState, useEffect } from 'react'

const menus = [
  { id: 1, name: '홈', url: '/' },
  { id: 2, name: '같이기부', url: '/together' },
  { id: 3, name: '모두의행동', url: '/' },
  { id: 4, name: '마음날씨', url: '/' },
  { id: 5, name: '캠페인', url: '/' },
]

export default function Navbar() {
  const [activeMenu, setActiveMenu] = useState(1)
  const [isSticky, setIsSticky] = useState(false);
  
  const handleScroll = () => {
    if (window.scrollY >= 55) {
      setIsSticky(true);
    } else {
      setIsSticky(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
  
  return (
    <nav className={`flex items-center justify-center bg-white h-[100%] ${isSticky ? 'sticky' : ''}`} aria-label="전체메뉴">
      <ul id="menu" className="flex items-center h-[100%] list-none">
        {menus?.map((menu) => (
          <li
            key={menu.id}
            className={`list-item relative border-b-[2px] ${activeMenu === menu.id ? "border-gray-800" : "border-white"} box-border leading-6 tracking-widest h-[100%]`}
          >
            <Link
              href={menu.url}
              className="inline-flex justify-center items-center box-border h-[100%] tracking-widest leading-6 text-[14px] pt-[5px] pb-[9px] px-[4px]"
              onClick={() => setActiveMenu(menu.id)}
            >
              {menu.name}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  )
}