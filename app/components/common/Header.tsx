'use client'

import React, { useState, useEffect, useRef } from 'react'
import Navbar from './Navbar'
import Image from 'next/image'
import Sidebar from './Sidebar'

export default function Header() {
  const [isOpenSidebar, setIsOpenSidebar] = useState(false)
  const modalRef = useRef<HTMLDivElement | null>(null);

  const handleBodyClick = (event: MouseEvent) => {
    if (modalRef.current && modalRef.current.contains(event.target as Node)) {
      return;
    }
    setIsOpenSidebar(false)
  };

  useEffect(() => {
    document.body.addEventListener('click', handleBodyClick);

    return () => {
      document.body.removeEventListener('click', handleBodyClick);
    };
  }, []);

  return (
    <header className="block h-[70px] top-0 right-0 box-border border-b border-gray-100 sticky bg-white z-[200] text-[17px]">
      <section className="flex justify-between items-center max-w-[1120px] h-full my-0 mx-auto min-w-[280px]">
        <h1 className="block m-0 p-0 border-0 align-baseline h-[70px]">
          <a href="/" className="flex items-center justify-center w-[112px] h-[100%]">
            옹옹<span className="font-semibold">같이가치</span>
          </a>
        </h1>
        <Navbar />
        <article className="util relative flex items-center" aria-label="유틸메뉴">
          <div className="relative flex">
            <a className="flex relative w-[24px] h-[24px] cursor-pointer">
              <div className="w-full h-full overflow-auto relative shrink-0">
                <figure>
                  <Image
                    src="../svgs/user.svg"
                    alt="profile"
                    width={25}
                    height={25}
                    priority={true}
                    className="object-cover"
                  />
                </figure>
              </div>
            </a>
          </div>
        </article>
        <a className="search" href="https://together.kakao.com/search" aria-label="검색">
          <div aria-hidden="true">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" width="20" height="20" opacity="1">
              <g fill="#202020" fillRule="nonzero">
                <path d="M18 3c8.284 0 15 6.716 15 15 0 8.284-6.716 15-15 15-8.284 0-15-6.716-15-15C3 9.716 9.716 3 18 3zm0 3C11.373 6 6 11.373 6 18s5.373 12 12 12 12-5.373 12-12S24.627 6 18 6z"></path>
                <path d="M26.94 27.05a1.5 1.5 0 0 1 2.007-.103l.114.103 9.434 9.435a1.5 1.5 0 0 1-2.007 2.224l-.114-.103-9.435-9.434a1.5 1.5 0 0 1 0-2.122z"></path>
              </g>
            </svg>
          </div>
        </a>
        <button className="sideMenu" type="button" color="transparent" onClick={() => setIsOpenSidebar(true)}>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" width="18" height="18" opacity="1">
            <g transform="translate(2 5)" fill="#22272b" fillRule="evenodd">
              <rect width="36" height="3" rx="1.5"></rect>
              <rect y="14" width="36" height="3" rx="1.5"></rect>
              <rect y="27" width="36" height="3" rx="1.5"></rect>
            </g>
          </svg>
        </button>
        {/* Sidebar */}
        {isOpenSidebar && <Sidebar />}
      </section>
    </header>
  )
}