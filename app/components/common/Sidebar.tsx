import React from 'react'
import LinkButton from '../micro-components/LinkButton'
import { Actions, Campaign, Donation, Weather } from '@/public/svgs/_index'
import data from '@/data/sidebar.json'

type SubmenuType = {
  id: number;
  url: string;
  name: string;
}

const Sidebar = () => {
  const { submenus } = data

  return (
    <article className="sidebar">
      <div className="flex flex-col box-border bg-white translate-x-0 max-w-[300px] min-w-[280px] p-[30px] h-full overflow-y-auto transition-transform ease-in-out duration-300">
        <div className="">
          <button className="inline-flex justify-start items-center box-border w-full h-auto p-0 mt-0 mr-0 ml-0 mb-[20px] border-none cursor-pointer">
            <div className="flex justify-center items-center flex-shrink-0 flex-[0_0_auto] overflow-hidden relative w-[40px] h-[40px]">
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                <path strokeLinecap="round" strokeLinejoin="round" d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z" />
              </svg>
            </div>
            <span className="block overflow-hidden text-left pl-[5px]">
              <strong>로그인하세요</strong>
            </span>
          </button>
          <ul className="flex items-center border rounded bg-gray-50">
            <li key={1} className="list-item relative flex-shrink-0 flex-grow">
              <a className="flex items-center justify-center gap-[4px] p-[12px] cursor-pointer text-[14px]" href="/my">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-5 h-5">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z" />
                </svg>MY
              </a>
            </li>
            <li key={2} className="split list-item relative flex-shrink-0 flex-grow">
              <a className="flex items-center justify-center gap-[4px] p-[12px] cursor-pointer text-[14px]" href="/notifications">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-4 h-4">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0M3.124 7.5A8.969 8.969 0 015.292 3m13.416 0a8.969 8.969 0 012.168 4.5" />
                </svg>
                알림
              </a>
            </li>
          </ul>
          <nav aria-label="사이드메뉴" className="block pt-[28px]">
            <ul className="flex flex-col items-start gap-[8px]">
              <li key={1} className="list-item relative text-[15px] w-full m-0">
                <LinkButton title="같이기부" url="donation"><Donation /></LinkButton>
              </li>
              <li key={2} className="list-item relative text-[15px] w-full m-0">
                <LinkButton title="모두의행동" url="actions"><Actions /></LinkButton>
              </li>
              <li key={3} className="list-item relative text-[15px] w-full m-0">
                <LinkButton title="마음날씨" url="hello"><Weather /></LinkButton>
              </li>
              <li key={4} className="list-item relative text-[15px] w-full m-0">
                <LinkButton title="캠페인" url="campaigns"><Campaign /></LinkButton>
              </li>
            </ul>
            <ul className="flex flex-wrap pt-[20px] pr-0 pl-0 pb-0 my-0 mx-[-18px]">
              {submenus.map((submenu: SubmenuType) => 
                <>
                  <li key={submenu.id} className="list-item relative text-[14px] my-0 mx-[18px]" style={{ width: 'calc(50% - 36px)' }}>
                    <a href={submenu.url} className="inline-flex items-center justify-start cursor-pointer py-[5px]">
                      {submenu.name}
                    </a>
                  </li>
                </>
              )}
            </ul>
          </nav>
        </div>
      </div>
      <div id="back" className="fixed inset-0 z-[-1] w-full h-full bg-black opacity-30"></div>
    </article>
  )
}

export default Sidebar
