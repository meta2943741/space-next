import React from 'react'
import Image from 'next/image'
import Link from 'next/link'
import data from '../../../data/footer.json';
import './Footer.css'

const Footer = () => {
  const { menus, sns } = data;

  return (
    <footer className={`block relative pt-[27px] pb-[29px] px-[20px] bg-[#F7F7F7] box-border border-t border-gray-75`}>
      <div className="flex flex-col items-center w-full h-full">
        <a href="/" className="text-[17px]">
          옹옹<span className="font-semibold ml-[2px]">같이가치</span>
        </a>
        <p className="text-[15px] text-gray-500 font-extralight pt-[10px] pb-[16px]">
          옹옹같이가치는 옹옹이와 옹옹메타가 함께합니다.
        </p>
        <div className="flex flex-col items-center gap-[16px] pt-[10px] border-t border-gray-75 w-full">
          <ul className="info flex items-center">
            {menus?.map((menu) => 
              <>
                <li key={menu.id} className="flex items-center relative text-[15px] text-gray-500">
                  <Link href={menu.url} target="_blank" className="inline-block">
                    <span>{menu.name}</span>
                  </Link>
                </li>
              </>
            )}
          </ul>
          <ul className="flex items-center justify-center gap-[10px]">
            {sns?.map((sns) => 
              <>
                <li key={sns.id} className="list-item relative w-[25px]">
                  <Link href={sns.url} target="_blank">
                    <figure className="flex items-center justify-center py-[6px] cursor-pointer">
                      <Image src={sns.icon} alt={sns.alt} width={sns.width} height={sns.height} priority={true} />
                    </figure>
                  </Link>
                </li>
              </>
            )}
          </ul>
          <a target="_blank" href="https://www.kakaocorp.com/main">
            <small className="text-[12px] text-gray-500 text-thin">© Ongong Corp.</small>
          </a>
        </div>
      </div>
    </footer>
  )
}

export default Footer
