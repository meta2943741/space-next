import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

type Props = {
  id: string,
  title: string,
  description: string,
  ariaLabel: string,
  price: string,
  percentage: string,
  hashtag: string,
  imageUrl: string,
}

const Card = ({
  id,
  title,
  description,
  ariaLabel,
  price,
  percentage,
  hashtag,
  imageUrl
}: Props) => {
  const dimensions = { width: 320, height: 220 };

  const formatMoney = (priceString: string): string => {
    const numericString = priceString.replace(/[^0-9.]/g, '');
    const price = parseFloat(numericString);

    if (!isNaN(price)) {
      return price.toLocaleString('ko-KR', { style: 'currency', currency: 'KRW' }) + '원';
    } else {
      return '0원';
    }
  };

  return (
    <div className="flex box-border" style={{ flexFlow: 'column wrap' }}>
      <div className="block h-auto md:h-[250px] w-full mt-0 mx-0 mb-[20px] md:mb-[30px] bg-white rounded-md box-border shadow-lg shadow-gray-300/40">
        <Link
          href="/"
          className="flex items-center h-full cursor-pointer flex-col md:flex-row"
          aria-label={ariaLabel}
        >
          <figure className="w-full h-[220px] object-cover md:w-[250px] md:h-full">
            <Image
              src={imageUrl}
              alt={title}
              priority={true}
              width={640}
              height={360}
              className={`rounded-t-md md:rounded-l-md md:rounded-r-none object-cover w-full md:w-[250px] h-[220px] md:h-full`}
            />
          </figure>
          <div className="flex flex-col w-full md:w-[calc(100%-250px)] box-border relative overflow-visible pt-[18px] px-[20px] pb-[23px] md:py-0 md:px-[30px] md:mt-[20px]">
            <strong className="text-[20px] font-thin pt-[2px] break-keep overflow-hidden flex-wrap text-ellipsis" style={{ display: '-webkit-flex' }}>
              {title}
            </strong>
            <span
              className="w-full text-ellipsis overflow-hidden mt-[5px] text-[15px] whitespace-normal font-thin text-gray-500 leading-6 line-clamp-3"
              style={{ overflowWrap: 'break-word' }}
            >
              {description}
            </span>
            <div className="block cursor-pointer mt-auto pt-[16px]">
              <div className="flex h-[3px] rounded-full w-full bg-[#E9E9E9]">
                <div className={`block h-full rounded-full bg-[#FF4050] cursor-pointer`} style={{ width: `${Number(percentage).toString()}%` }}></div>
              </div>
              <div className="flex justify-between pt-[4px]">
                <span className="text-[15px] leading-5">{formatMoney(price)}</span>
                <span className="text-[15px] leading-5 text-[#FF4050]">{percentage}%</span>
              </div>
            </div>
            <button
              className="md:order-1 md:bg-transparent md:border-none absolute left-[20px] top-[-40px] w-fit pt-[1px] pb-[3px] px-[8px] rounded-md inline-flex box-border items-center justify-center border m-0 border-gray-900 bg-gray-900 text-white text-[13px] text-thin opacity-90 md:mt-[18px]"
              type="button"
              name={hashtag}
            >
              <span className="md:text-[#7c7878]">#</span>
              <span className="md:text-[#7c7878]">{hashtag}</span>
            </button>
          </div>
        </Link>
      </div>
    </div>
  )
}

export default Card