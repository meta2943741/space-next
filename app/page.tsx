/* eslint-disable react/jsx-key */
/* eslint-disable react/no-unescaped-entities */
import Card from "./components/Card";
import data from '../data/together.json';

export default function Home() {
  const { togethers } = data;

  return (
    <main id="home" className={`min-h-[100vh] bg-[#F3F3F3]`}>
      <section id="card-header-wrapper" className="block overflow-hidden my-0 mx-auto pt-[22px]">
        <h3 className="py-0 px-[20px] whitespace-pre-line text-[22px]">
          당신의 착한 마음을
          같이가치가 응원합니다❤️
        </h3>
      </section>
      <div id="card-wrapper" className="flex mt-[29px] mb-0 mx-auto">
        <section id="card-left-section" className="block md:w-[700px]">
          <div>
            <div className="flex flex-col border-box">
              {togethers.map(together => (
                <Card {...together} />
              ))}
            </div>
          </div>
        </section>
        <section id="card-right-section" className="block md:w-[360px]">
          <div className="mb-[20px] md:mb-[30px] rounded-md shadow-md bg-white">
            <div className="relative bg-yellow-300 rounded-t-md pt-[23px] md:pt-[34px] pb-[15px] md:pb-[29px] px-[20px] md:px-[26px]">
              <h4>
                <a href="https://together.kakao.com/auroville/statistics" className="inline-block align-top">
                  <span className="text-[20px]">우리가
                    <br />같이만든 변화들
                  </span>
                </a>
              </h4>
              <p className="pt-[10px] md:pt-[12px] text-[13px] text-gray-500">2023.10.22 기준</p>
              <div></div>
              <div>
                <a href="https://together.kakao.com/auroville/statistics" className="block align-top">
                  <div className="flex mt-[24px] md:mt-[41px] h-[50px] md:h-[60px] py-0 md:py-0 px-[14px] md:px-[20px] w-[full] items-center box-border bg-white">
                    <svg viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" className="w-[20px] h-[20px] mr-[8px]">
                      <rect fill="none" height="256" width="256" />
                      <line fill="none" stroke="#000" strokeLinecap="round" strokeLinejoin="round" strokeWidth="12" x1="16" x2="240" y1="136" y2="136" />
                      <polyline fill="none" points="24 72 76 200 128 72 180 200 232 72" stroke="#000" strokeLinecap="round" strokeLinejoin="round" strokeWidth="12" />
                    </svg>
                    <strong className="text-[15px]">총 기부금</strong>
                    <em className="text-[16px] md:text-[18px] grow text-right">71,491,587,824원</em>
                  </div>
                </a>
              </div>
            </div>
            <div className="bg-white pt-[30px] md:pt-[31px] pb-[20px] md:pb-[26px] px-[20px] md:px-[26px] rounded-b-md box-border md:h-[281px]">
              <div>
                <a href="https://together.kakao.com/fundraisings/epilogue" className="inline-block align-top">
                  <h4 className="text-[14px] text-gray-400">따뜻한 후기</h4>
                </a>
              </div>
              <div className="block">
                <div className="relative block">
                  <div className="absolute w-[1px] h-[1px] overflow-hidden p-0 m-[-1px] border-none">
                    Slide 1 of 3
                  </div>
                  <div className="items-end justify-items-end absolute flex z-[1] top-0 left-0 bottom-0 right-0 pointer-events-none">
                    <div className="block md:w-full pointer-events-auto">
                      <ul className="flex md:relative md:top-[20px] md:justify-center">
                        <li className="list-item">
                          <button type="button" aria-label="슬라이드 1/3" aria-current="true"></button>
                        </li>
                        <li>
                          <button type="button" aria-label="슬라이드 2/3" aria-current="false"></button>
                        </li>
                        <li>
                          <button type="button" aria-label="슬라이드 3/3" aria-current="false"></button>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="overflow-hidden w-full relative outline-none h-auto will-change-contents">
                    <div className="text-left flex">
                      <div>
                        <a href="https://together.kakao.com/fundraisings/105223/news">
                          <div>
                            <strong className="overflow-hidden text-ellipsis md:pt-[16px] md:max-h-[56px] pt-[20px]">
                              기다림의 끝에 온유에게 찾아온 선물
                            </strong>
                            <p className="pt-[15px] text-[15px] text-gray-600 break-all break-words line-clamp-5 leading-7 md:max-h-[135px] md:pt-[11px]">
                              온유는 선천성 심장질환으로 생후 10일 만에 심장 수술을 받았습니다. 장기 입원 생활 속에서 온유는 힘든 병원 생활을 누구보다 씩씩하게 지냈습니다. 가족, 의료진들의 응원과 사랑 속에 또래보다 느리지만 조금씩 성장해갔고, 사계절과 첫돌을 병원에서 맞이했습니다.
                            </p>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </main>
  )
}
