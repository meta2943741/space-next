/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      zIndex: {
        '100': '100',
        '200': '200',
        '300': '300',
        '400': '400',
        '500': '500',
      },
      flex: {
        '0-0-auto': '0 0 auto',
        '1-0-auto': '1 0 auto',
      },
      color: {
        'bg-gray-50': '#F7F7F7',
        'bg-gray-75': '#EEEEEE',
      },
      minHeight: {
        '100vh': 'calc(100vh - 244px)'
      }
    },
  },
  plugins: [],
}